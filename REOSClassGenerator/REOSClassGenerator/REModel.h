//
//  REModel.h
//  REOSClassGenerator
//
//  Created by Aron Bury on 4/07/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface REModel : NSObject

- (BOOL)isValid;

@end

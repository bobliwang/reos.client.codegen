//
//  AppDelegate.m
//  REOSClassGenerator
//
//  Created by Aron Bury on 3/04/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import "AppDelegate.h"
#import "ClassGenerator.h"
#include <stdlib.h>
#include <stdio.h>
#import <FMDatabase.h>
#import <FMResultSet.h>

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self checkEntitiesExistence];
    
    NSArray *array = @[
                       @"IdentityRanges",
                       @"ReplSettings",
                       @"FullDistributorAccountProducts",
                       @"FullOrderItem",
                       @"SinglePromotion",
                       @"FullDistributorAccounts",
                       @"FullOrders",
                       @"REOSOdometerRecord",
                       @"FullObjectiveAccountExtds",
                       @"FullCompetitorActivity",
                       @"FullPayrollTimesheets",
                       @"FullPayrolls",
                       @"FullPayrollLeaves",
                       @"FullContact",
                       @"FullToDo",
                       @"FullAccount",
                       @"FullCDA",
                       @"FullCallPreference",
                       @"ProductDistributor",
                       @"FullPhoto",                       
                       @"FullPOSRequest",
                       @"FullRelayBooking",
                       @"FullDistCallProduct",
                       @"FullOSHHazard",
                       @"FullOshIncident",
                       @"ScreenOption",
                       @"FullAsset",
                       @"FullSurvey",
                       @"FullNews",
                       @"FullCall",
                       @"FullCallActivity",
                       @"FullCallActivityAdhoc",                       
                       @"FullFIA",
                       @"FullAccountProducts",
                       @"FullDistCallSubCategory",
                       @"FIAActivityOption",                       
                       @"Promotion",
                       @"FullProduct",
                       @"FullObjective",                       
                       @"FullAccountBay",
                       @"ReplInserts",
                       @"ReplDeletes",
                       @"ReplUpdatedColumns",
                       @"ReplUpdates",
                       @"Distributors",
                       @"SingleObjective",
                       @"SurveyAccounts",
                       @"Lookups",
                       @"OshIncidents",
                       @"CallPreferences",
                       @"Fias",
                       @"Countries",
                       @"SurveyResultAnswers",
                       @"CallActivityAdhocs",
                       @"ObjectiveLines",
                       @"CallActivityUsers",
                       @"AccountBays",
                       @"PayrollTimesheets",
                       @"PayrollLeaves",
                       @"AccountSubCategories",
                       @"CallActivities",
                       @"POSRequests",
                       @"SurveyQuestions",
                       @"AccountExtds",
                       @"Calls",
                       @"ObjectiveAccountExtds",
                       @"OshRiskMatrixes",
                       @"SurveyResults",
                       @"AccountProducts",
                       @"CDAScorecardLines",
                       @"Products",
                       @"StoreWalks",
                       @"Accounts",
                       @"NewsUsers",
                       @"States",
                       @"CompetitorActivities",
                       @"DistCallSubCategories",
                       @"OshHazards",
                       @"Surveys",
                       @"DistCallProducts",
                       @"Contacts",
                       @"ToDos",
                       @"ExtdCategories",
                       @"StoreWalkExtds",
                       @"ExtdSubCategories",
                       @"ExtdColumns",
                       @"News",
                       @"ObjectiveAccounts",
                       @"DistributorAccountProducts",
                       @"Objectives",
                       @"AccountNotes",
                       @"SurveyAnswers",
                       @"PayrollOdometers",
                       @"DistributorAccounts",
                       @"OrderItems",
                       @"CDAScorecards",
                       @"RelayBookings",
                       @"Users",
                       @"Photos",
                       @"CDAs",
                       @"FiaExtds",
                       @"Assets",
                       @"Payrolls",
                       @"UserAccounts",
                       @"Orders",
                       @"POSRequestItems",
                       @"FullCreditNotes",
                       @"FullCreditNoteItem",
                       @"FullOrderCreditNote",
                       @"FullCreditNoteSummaries",
                       @"FullOrderItem",
                       @"OrderItems",
                       @"CreditNotes",
                       @"CreditNoteItems",
                       @"CreditNoteSummaries"];
    
    for (NSString *tableName in array)
        [ClassGenerator generatClassWithTableName:tableName];
}

- (void)checkEntitiesExistence
{
    NSLog(@"CREATING INITAL VIEWS");
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Views" ofType:@"plist"];
    NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"db"];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSArray *views = [NSArray arrayWithContentsOfFile:path];
    
    for (int k = 0; k < views.count; k++) {
        NSDictionary *eachView = views[k];
        
        NSString *entityName = eachView[@"Name"];
        NSString *query = [NSString stringWithFormat:@"SELECT Count(*) FROM sqlite_master WHERE name='%@';", entityName];
        FMResultSet *set = [database executeQuery:query];
        int count = 0;
        if ([set next])
            count = [set intForColumnIndex:0];

        if (count != 1)
        {
            NSString *insertQuery = eachView[@"Value"];
            if (insertQuery.length > 0)
                [database executeUpdate:insertQuery];
        }
        else
        {
            NSLog(@"Warning, %@ already exists, may be out of date", entityName);
        }
    }
    
    [database close];
}

@end

//
//  ClassGenerator.h
//  REOSClassGenerator
//
//  Created by Aron Bury on 3/04/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ClassGenerator;

@interface ClassGenerator : NSObject

+ (void)generatClassWithTableName:(NSString *)tableName;

@property (nonatomic, strong) NSString *tableName;

- (void)generateClass;

@end

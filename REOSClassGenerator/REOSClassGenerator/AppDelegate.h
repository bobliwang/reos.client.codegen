//
//  AppDelegate.h
//  REOSClassGenerator
//
//  Created by Aron Bury on 3/04/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end

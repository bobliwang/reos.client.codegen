//
//  main.m
//  REOSClassGenerator
//
//  Created by Aron Bury on 3/04/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}

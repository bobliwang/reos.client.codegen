//
//  ClassGenerator.m
//  REOSClassGenerator
//
//  Created by Aron Bury on 3/04/13.
//  Copyright (c) 2013 Airloom. All rights reserved.
//

#import "ClassGenerator.h"
#import <FMDatabase.h>
#import <FMResultSet.h>

#define kDataTypeInt @"INTEGER"
#define kDataTypeString @"NVARCHAR"
#define kDataTypeDecimal @"DECIMAL"
#define kDataTypeBlob @"BLOB"

#define kClassName          @"ClassName"
#define kClassProperties    @"Properties"

#define kPropertyName       @"PropertyName"
#define kPropertyDateType   @"DataType"

@interface ClassGenerator()

@property (nonatomic, strong) NSString *className;
@property (nonatomic, strong) FMDatabase *database;
@property (nonatomic, strong) NSFileHandle *hFileHandle;
@property (nonatomic, strong) NSFileHandle *mFileHandle;
@property (nonatomic, strong) NSArray *exceotionalClasses;

- (void)createFiles;

- (void)writeHeaderForHFile;
- (void)writeHeaderForMFile;

@end

@implementation ClassGenerator

+ (void)generatClassWithTableName:(NSString *)tableName
{
    ClassGenerator *classGenerator = [[ClassGenerator alloc] init];
    classGenerator.tableName = tableName;
    
    [classGenerator generateClass];
}

- (id)init
{
    self = [super init];
    if (self)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ClassesException" ofType:@"plist"];
        self.exceotionalClasses = [NSArray arrayWithContentsOfFile:path];
    }
    return self;
}

- (void)generateClass
{
    self.className = self.tableName;
    if ([self.className isEqualToString:@"FullPayrollTimesheets"])
    {
        DLog(@"Found");
    }
    [self createFiles];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"db"];
    self.database = [FMDatabase databaseWithPath:path];
    
    
    [self.database open];
    NSString *query = [NSString stringWithFormat:@"PRAGMA table_info(%@);", self.tableName];
    FMResultSet *attributes = [self.database executeQuery:query];
    
    [self writeHeaderForHFile];
    
    while ([attributes next])
    {
        
        NSString *attributeName = [attributes objectForColumnName:@"name"];
        if (![self checkException:attributeName])
        {
            if (attributeName.length > 2)
                if ([[attributeName substringToIndex:3] isEqualToString:@"New"])
                    attributeName = [NSString stringWithFormat:@"a%@", attributeName];
            
            NSString *firstLowercaseString = [[attributeName substringToIndex:1] lowercaseString];
            attributeName = [attributeName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstLowercaseString];
            
            NSString *attributeType = [attributes objectForColumnName:@"type"];
            
            BOOL isDate = NO;
            if ([attributeName rangeOfString:@"Date" options:NSCaseInsensitiveSearch].location != NSNotFound ||
                [attributeName rangeOfString:@"Time" options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                isDate = YES;
            }
            
            BOOL isNumber = [attributeType rangeOfString:kDataTypeInt].location != NSNotFound;
            BOOL isString = [attributeType rangeOfString:kDataTypeString].location != NSNotFound;
            BOOL isBlob = [attributeType rangeOfString:kDataTypeBlob].location != NSNotFound;
            BOOL isDouble = [attributeType rangeOfString:kDataTypeDecimal].location != NSNotFound;
            
            if (isDate)
                attributeType = @"NSDate";
            else if (isNumber || isDouble)
                attributeType = @"NSNumber";
            else if (isString)
                attributeType = @"NSString";
            else if (isBlob)
                attributeType = @"NSData";
            else
                attributeType = @"NSString";
            NSString *propertyDecleration = [NSString stringWithFormat:@"@property (nonatomic, strong) %@ *%@;\n", attributeType, attributeName];
            
            [self.hFileHandle writeData:[propertyDecleration dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    [self.hFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@"@end" dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Now populate .m
    [self writeHeaderForMFile];
}



- (BOOL)checkException:(NSString*)attributeName
{
    for (int j=0; j<_exceotionalClasses.count; j++) {
        NSDictionary* classDict = _exceotionalClasses[j];
        NSString* eachClassTitle = classDict[kClassName];
        if ([eachClassTitle isEqualToString:self.className]) {
            
            NSString *firstLowercaseString = [[attributeName substringToIndex:1] lowercaseString];
            attributeName = [attributeName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstLowercaseString];
            
            NSArray* properties = classDict[kClassProperties];
            for (int k=0; k<properties.count; k++) {
                NSDictionary* propertyDict = properties[k];
                NSString* propertyName = propertyDict[kPropertyName];
                if ([propertyName isEqualToString:attributeName]) {
                    
                    NSString* propertyDataType = propertyDict[kPropertyDateType];
                    
                    NSString *propertyDecleration = [NSString stringWithFormat:@"@property (nonatomic, strong) %@ *%@;\n", propertyDataType, attributeName];
                    
                    [self.hFileHandle writeData:[propertyDecleration dataUsingEncoding:NSUTF8StringEncoding]];
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

#pragma mark -
#pragma mark - Private Methods
- (void)createFiles
{
    const char *homeDir = getenv("HOME");
    NSString *homeDirStr = [NSString stringWithFormat:@"%s/Desktop/REOSClasses", homeDir];
    
    NSString *hFilePath = [NSString stringWithFormat:@"%@/%@.h", homeDirStr, self.className];
    NSString *mFilePath = [NSString stringWithFormat:@"%@/%@.m", homeDirStr, self.className];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:homeDirStr withIntermediateDirectories:NO attributes:nil error:nil];
    [[NSFileManager defaultManager] createFileAtPath:hFilePath contents:nil attributes:nil];
    [[NSFileManager defaultManager] createFileAtPath:mFilePath contents:nil attributes:nil];
    
    self.hFileHandle = [NSFileHandle fileHandleForWritingAtPath:hFilePath];
    self.mFileHandle = [NSFileHandle fileHandleForWritingAtPath:mFilePath];
}

- (void)writeHeaderForHFile
{
    [self.hFileHandle seekToEndOfFile];
    [self.hFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[[NSString stringWithFormat:@"//  %@.h\n", self.className] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@"//  REOSClassGenerator\n" dataUsingEncoding:NSUTF8StringEncoding]];;
    [self.hFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[[NSString stringWithFormat:@"//  Created by ClassGenerator on %@.\n", [NSDate date]] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@"//  Copyright (c) 2013 Airloom. All rights reserved.\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@"#import \"REModel.h\"" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];    
    [self.hFileHandle writeData:[[NSString stringWithFormat:@"@interface %@ : REModel\n", self.className] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.hFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)writeHeaderForMFile
{
    [self.mFileHandle seekToEndOfFile];
    [self.mFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[[NSString stringWithFormat:@"//  %@.m\n", self.className] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@"//  REOSClassGenerator\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[[NSString stringWithFormat:@"//  Created by ClassGenerator on %@.\n", [NSDate date]] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@"//  Copyright (c) 2013 Airloom. All rights reserved.\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@"//\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[[NSString stringWithFormat:@"#import \"%@.h\"\n", self.className] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];    
    [self.mFileHandle writeData:[[NSString stringWithFormat:@"@implementation %@\n", self.className] dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@" \n" dataUsingEncoding:NSUTF8StringEncoding]];
    [self.mFileHandle writeData:[@"@end" dataUsingEncoding:NSUTF8StringEncoding]];    
}
@end
